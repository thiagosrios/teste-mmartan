# Test Ammo Varejo

Este repositório contém a aplicação teste para vaga de desenvolvedor web da AMMO Varejo. 

O projeto é constituído por um módulo *frontend* usando ReactJs e um módulo *backend*, disponibilizado como uma API usando NodeJs,  Express e MongoDB. 

O projeto pode ser visualizado em ambiente de produção nesse [link](http://68.183.71.22:3000). 

## Dependências de execução do projeto

- NodeJs (versão 8 ou superior)
- MongoDB
- ReactJs

## Configuração local do projeto

Após instalar as dependências de execução, rode os comandos necessários para subir a aplicação, separadamente em cada pasta

### Configuração do backend

A aplicação é configurada para ser executada na porta 4000, diferente do padrão do Node (o React usará a porta 3000). 

- Entre no repositório backend/ 
- Inicie o serviço do MongoDB, usando o comando: 
	`$ sudo service mongod start`
- Entre na pasta backend a partir da pasta raiz do projeto
- Instale as dependências do projeto executando o seguinte comando no terminal: 
	`$ npm install`
- Execute a aplicação usando os comandos: 
	`$ npm run start` ou `$ npm run live` (para exibir log de requisições, via Nodemon). 

### Configuração do frontend

- Entre no repositório frontend/ 
- Instale as dependências do projeto executando o seguinte comando no terminal: 
	`$ npm install`
- Execute a aplicação usando os comandos: 
	`$ npm run start`