import React, { Component } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, Button, Form, FormGroup, Input } from 'reactstrap';
import Logo from './Logo';

class Toolbar extends Component {
  
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            term: '',
            search: '', 
            isOpen: false
        };
    }

    handleChange = event => {
        const {name, value} = event.target;

        this.setState({
            [name] : value
        });
    }

    submitForm = () => {
        const { term } = this.state; 
        this.setState({ search: term });
        this.props.searchEvent(term);
    }

    toggle() {
        this.setState({ isOpen: !this.state.isOpen });
    }

    render() {
        const { search } = this.state;
        const { term } = this.state;

        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">
                        <Logo />
                    </NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <Form inline>
                                    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                                        <Input 
                                            type="text" 
                                            name="term" 
                                            value={term} 
                                            placeholder="Pesquise um produto"
                                            onChange={this.handleChange} />
                                    </FormGroup>
                                    <Button onClick={this.submitForm}>
                                        Pesquisar   
                                    </Button>
                                </Form>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
                <h1 className="search-header">{search}</h1>
            </div>
        );
    }
}

export default Toolbar; 