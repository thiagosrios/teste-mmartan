import React, { Component } from 'react';
import Toolbar from './Toolbar'
import Results from './Results'; 

class App extends Component {
	
	state = {
		search: ''
	}; 

  	searchEvent = submit => {
        this.setState({ search: submit });
        this.child.searchAPI(submit);
    }

  	render() {
    	return (
    		<div>
      			<Toolbar searchEvent={this.searchEvent} />
      			<Results onRef={ref => (this.child = ref)} />
      		</div>
    	);
  	}
}

export default App;
