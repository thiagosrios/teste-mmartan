import React, { Component } from 'react';
import { Container, Row, Col, ListGroup, ListGroupItem } from 'reactstrap';
import PaginateButtons from './PaginateButtons'; 
import Select from './Select'; 

class Results extends Component {

    state = {
        search: '', 
        perPage: 16,
        pages: 1, 
        currentPage: 0,
        data: []
    }

    // Callback executado quando o componente é inicializado
    componentDidMount() {
        this.props.onRef(this);
    }
    
    // Callback executado quando o componente é desativado
    componentWillUnmount() {
        this.props.onRef(undefined);
    }

    // Recebe evento onChange do select e atualiza state
    handleSelect = value => {
        this.setState({
            perPage: value
        });
    }

    // Método para realizar pesquisa por termos a partir da busca
    searchAPI = term => {
        if(term !== "") {
            let url = "http://localhost:4000/produtos/busca/"  + term;

            fetch(url)   
                .then(result => result.json())
                .then(result => {
                    this.setState({
                        data: result
                    })
                });
        }
    }

    // Retorna lista de itens para exibição dos resultados
    createList(data) {
        let itens = data.map((row, key) => {
            let image = 'http://localhost:4000/images/' + row.imagem; 
            return (
                <ListGroupItem key={key}>
                    <Row>
                        <Col xs="2">
                            <img src={image} alt="" className="img-responsive" width={100} />
                        </Col>
                        <Col xs="9" className="padding-text">
                            <span>{row.nome} - {row.descricao}</span>
                        </Col>
                        <Col xs="1" className="padding-text">
                            <span>R$ {row.preco}</span>
                        </Col>
                    </Row>
                </ListGroupItem>
            );
        });

        return itens;
    }

    // Método principal para exibir os elementos do componente
    render() {

        const { data } = this.state; 
        let resultados = data.length; 
        
        if(resultados > 0) {
            // this.definePagination(data, resultados); 
            const { perPage } = this.state; 
            let paginas = Math.ceil(resultados / perPage); 
            // console.log(data.slice(0, (perPage-1)));

            let headerResultados = resultados > 1 ? ' resultados encontrados' : ' resultado encontrado'; 
            headerResultados = resultados + headerResultados; 

            return (
                <div>
                    <Container>
                        <Row>
                            <h3 className="mt-4 mb-4 pl-3">{headerResultados}</h3>
                        </Row>
                        <Row>
                            <Col xs="12">
                                <ListGroup className="mb-6">{this.createList(data)}</ListGroup>
                            </Col>
                        </Row>
                        <Row className="mt-6">
                            <Col xs="3" className="mt-4">
                                <Select handleSelect={this.handleSelect} />
                            </Col>
                            <Col xs="6"></Col>
                            <Col xs="3" className="mt-4 float-left">
                                <PaginateButtons currentPage={this.state.currentPage} pages={paginas} />
                            </Col>
                        </Row>
                    </Container>
                </div>
            );
        } else {
            return(
                <div>
                    <Container>
                        <p className="text-center">Nenhum resultado encontrado</p>
                    </Container>
                </div>
            );
        }
    }
}

export default Results;  