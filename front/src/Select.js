import React, { Component } from 'react';
import { FormGroup, Input } from 'reactstrap';

class Select extends Component {
	
	// Atualiza a informação de quantidade de páginas do state de Results
    onChange = event => {
        this.props.handleSelect(event.target.value); 
    }

	render() {
		const quantidades = [16, 32, 48, 64, 80]; 

		const options = quantidades.map((value, key) => {
			return(
				<option key={key} value={value}>{value} produtos por página</option>
			);
		}); 

		return(
			<FormGroup>
	          	<Input type="select" name="select" onChange={this.onChange}>
	            	{options}
	          	</Input>
	        </FormGroup>
		);
	}

}

export default Select;