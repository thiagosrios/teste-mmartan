import React, { Component } from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

class PaginateButtons extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            currentPage: this.props.currentPage
        };
    }

    render() {
        const pages = this.props.pages;

        var links = [] 
        for (var i = 0; i < pages; i++) {
            links.push(
                <PaginationItem key={i} active={this.props.currentPage === i ? true : false}>
                    <PaginationLink href="#">{ i + 1 }</PaginationLink>
                </PaginationItem>
            );
        }

        return (
            <Pagination children={links} aria-label="Paginação">
                <PaginationItem>
                    <PaginationLink previous href="#" />
                </PaginationItem>
                { links }
                <PaginationItem>
                    <PaginationLink next href="#" />
                </PaginationItem>
            </Pagination>
        );
    }
}

export default PaginateButtons; 