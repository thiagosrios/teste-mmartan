const ProdutosController = require('../api/produtos/produtos.controller');

exports.routesConfig = function (app) {
    app.post('/produtos', [ ProdutosController.create ]);
    app.get('/produtos', [ ProdutosController.list ]);
    app.get('/produtos/busca/:term', [ ProdutosController.search ]);
    app.get('/produtos/:id', [ ProdutosController.read ]);
    app.patch('/produtos/:id', [ ProdutosController.update ]);
    app.delete('/produtos/:id', [ ProdutosController.delete ]);
};