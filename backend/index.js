#!/usr/bin/env node

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const config = require('./config/env.config');
const routes = require('./config/routes.config');

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, X-Requested-With, Range');
    if (req.method === 'OPTIONS') {
        return res.send(200);
    } else {
        return next();
    }
});

app.use(express.static('public'));

app.use(bodyParser.json());
routes.routesConfig(app);

app.listen(config.port, function () {
    console.log('Aplicação executando na porta %s', config.port);
});