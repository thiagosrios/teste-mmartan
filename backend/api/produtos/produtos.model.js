const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/rest', { useNewUrlParser: true });
const Schema = mongoose.Schema;

const produtoSchema = new Schema({
    nome: String,
    descricao: String,
    preco: Number,
    quantidade: Number, 
    imagem: String
});

// Ensure virtual fields are serialised.
produtoSchema.set('toJSON', {
    virtuals: true
});

produtoSchema.virtual('id').get(function () {
    return this._id.toHexString();
});

produtoSchema.findById = function (cb) {
    return this.model('Produtos').find({id: this.id}, cb);
};

const Produto = mongoose.model('Produtos', produtoSchema);

exports.findById = (id) => {
    return Produto.findById(id).then(
        (result) => {
            result = result.toJSON();
            delete result._id;
            delete result.__v;
            return result;
        }, 
        (error) => {
            var err = { message: "Produto não encontrado" }; 
            return err; 
        });
};

exports.findByName = (nome) => {
    return Produto.find({ nome: new RegExp(nome, "i")}).then(
        (result) => {
            return result;
        }, 
        (error) => {
            var err = { message: "Produto(s) não encontrado" }; 
            return err; 
        });
};

exports.add = (data) => {
    const produto = new Produto(data);
    result = produto.save();
    delete result.__v;
    return result;
};

exports.list = (perPage, page) => {
    return new Promise((resolve, reject) => {
        Produto.find()
            .limit(perPage)
            .skip(perPage * page)
            .exec(function (err, Produtos) {
                if (err) {
                    reject(err);
                } else {
                    resolve(Produtos);
                }
            })
    });
};

exports.update = (id, data) => {
    return new Promise((resolve, reject) => {
        Produto.findById(id, function (err, produto) {
            if (err) reject(err);
            for (let i in data) {
                produto[i] = data[i];
            }
            produto.save(function (err, updatedProduto) {
                if (err) return reject(err);
                resolve(updatedProduto);
            });
        });
    });
};

exports.delete = (id) => {
    return new Promise((resolve, reject) => {
        Produto.deleteOne({_id: id}, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(err);
            }
        });
    });
};
