const ProdutoModel = require('./produtos.model');

exports.create = (req, res) => {
    ProdutoModel.add(req.body).then((result) => {
        res.status(201).send({id: result._id});
    });
};

exports.list = (req, res) => {
    let limit = req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10;
    let page = 0;
    
    if (req.query) {
        if (req.query.page) {
            req.query.page = parseInt(req.query.page);
            page = Number.isInteger(req.query.page) ? req.query.page : 0;
        }
    }

    ProdutoModel.list(limit, page).then((result) => {
        res.status(200).send(result);
    });
};

exports.read = (req, res) => {
    ProdutoModel.findById(req.params.id).then(
        (result) => { res.status(200).send(result); }, 
        (error) => { res.status(204).send(error); }
    ); 
};

exports.update = (req, res) => {
    ProdutoModel.update(req.params.id, req.body).then((result) => {
        res.status(204).send({ message: "Produto atualizado" });
    });
};

exports.delete = (req, res) => {
    ProdutoModel.delete(req.params.id).then((result) => {
        res.status(204).send({ message: "Produto excluído" });
    });
};

exports.search = (req, res) => {
    ProdutoModel.findByName(req.params.term).then(
        (result) => { res.status(200).send(result); }, 
        (error) => { res.status(204).send(error); }
    ); 
}